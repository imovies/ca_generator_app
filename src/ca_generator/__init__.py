import os
from pathlib import Path
import subprocess
import tempfile

from flask import Flask, jsonify, request


CA_INFO = "/" + "/".join(
    [
        "C=CH",
        "ST=Vaud",
        "L=Lausanne",
        "O=iMovies",
        "CN=imovies.ch",
    ]
)

OPENSSL = "/run/current-system/sw/bin/openssl"


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=False, instance_path='/var/lib/ca_generator_app')

    # ASL book 7.5 Running a certificate authority
    ca_path = Path(app.instance_path)
    cert_path = ca_path / "cacert.pem"
    serial_path = ca_path / "serial"
    index_path = ca_path / "index.txt"
    config_path = ca_path / "openssl.cnf"
    privkey_path = ca_path / "private/cakey.pem"
    if not cert_path.exists():
        serial_path.write_text("01")
        index_path.touch()
        (ca_path / "private").mkdir(0o700, exist_ok=True)
        (ca_path / "newcerts").mkdir(0o700, exist_ok=True)
        config_path.write_text(
            f"""
[ ca ]
default_ca	= iMovies_email_CA

[iMovies_email_CA]
dir		= {ca_path}		# Where everything is kept
certs		= $dir/certs		# Where the issued certs are kept
crl_dir		= $dir/crl		# Where the issued crl are kept
database	= $dir/index.txt	# database index file.
unique_subject	= no			# Set to 'no' to allow creation of
					# several certs with same subject.
new_certs_dir	= $dir/newcerts		# default place for new certs.

certificate	= $dir/cacert.pem 	# The CA certificate
serial		= $dir/serial 		# The current serial number
crlnumber	= $dir/crlnumber	# the current crl number
					# must be commented out to leave a V1 CRL
crl		= $dir/crl.pem 		# The current CRL
private_key	= $dir/private/cakey.pem # The private key

x509_extensions	= usr_cert		# The extensions to add to the cert

rand_serial = no
email_in_dn = yes

# Comment out the following two lines for the "traditional"
# (and highly broken) format.
name_opt 	= ca_default		# Subject Name options
cert_opt 	= ca_default		# Certificate field options

# Extension copying option: use with caution.
# copy_extensions = copy

# Extensions to add to a CRL. Note: Netscape communicator chokes on V2 CRLs
# so this is commented out by default to leave a V1 CRL.
# crlnumber must also be commented out to leave a V1 CRL.
# crl_extensions	= crl_ext

default_days	= 365			# how long to certify for
default_crl_days= 30			# how long before next CRL
default_md	= default		# use public key default MD
preserve	= no			# keep passed DN ordering

# A few difference way of specifying how similar the request should look
# For type CA, the listed attributes must be the same, and the optional
# and supplied fields are just that :-)
policy		= policy_match

# For the CA policy
[ policy_match ]
countryName		= match
stateOrProvinceName	= match
organizationName	= match
organizationalUnitName	= optional
commonName		= supplied
emailAddress		= optional

[ usr_cert ]

# These extensions are added when 'ca' signs a request.

# This goes against PKIX guidelines but some CAs do it and some software
# requires this to avoid interpreting an end user certificate as a CA.

basicConstraints=CA:FALSE

# This is typical in keyUsage for a client certificate.
# keyUsage = nonRepudiation, digitalSignature, keyEncipherment

# PKIX recommendations harmless if included in all certificates.
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer

# This stuff is for subjectAltName and issuerAltname.
# Import the email address.
# subjectAltName=email:copy
# An alternative to produce certificates that aren't
# deprecated according to PKIX.
# subjectAltName=email:move

# Copy subject details
# issuerAltName=issuer:copy

# This is required for TSA certificates.
# extendedKeyUsage = critical,timeStamping
        """
        )
        subprocess.run(
            [
                OPENSSL,
                "req",
                "-passin",
                "pass:",
                "-passout",
                "pass:",
                "-new",
                "-x509",
                "-extensions",
                "v3_ca",
                "-keyout",
                privkey_path,
                "-out",
                cert_path,
                "-days",
                "3650",
                "-subj",
                CA_INFO,
            ],
            check=True,
        )

    @app.post("/personal_certificate/create")
    def create_cert():
        email = request.json["email"]
        name = request.json["name"]
        with tempfile.TemporaryDirectory() as tmpdir:
            subprocess.run(
                [OPENSSL, "genrsa", "-out", "user.key"], cwd=tmpdir, check=True
            )
            subprocess.run(
                [
                    OPENSSL,
                    "req",
                    "-new",
                    "-key",
                    "user.key",
                    "-out",
                    "user.csr",
                    "-subj",
                    f"/C=CH/ST=Vaud/L=Lausanne/O=iMovies/CN={name}/emailAddress={email}",
                ],
                cwd=tmpdir,
                check=True,
            )
            subprocess.run(
                [
                    OPENSSL,
                    "ca",
                    "-batch",
                    "-config",
                    config_path,
                    "-in",
                    "user.csr",
                    "-passin",
                    "pass:",
                    "-out",
                    "user.pem",
                ],
                cwd=tmpdir,
                check=True,
            )
            return jsonify(
                {
                    "private_key": Path(tmpdir, "user.key").read_text(),
                    "certificate": Path(tmpdir, "user.pem").read_text(),
                }
            )

    @app.post("/personal_certificate/revoke")
    def revoke_cert():
        cert = request.json["certificate"]
        with tempfile.TemporaryDirectory() as tmpdir:
            Path(tmpdir, "cert.pem").write_text(cert)
            subprocess.run(
                [
                    OPENSSL,
                    "ca",
                    "-batch",
                    "-config",
                    config_path,
                    "-revoke",
                    "cert.pem",
                    "-passin",
                    "pass:",
                ],
                cwd=tmpdir,
                check=True,
            )
        return jsonify({})

    @app.get("/personal_certificate/index")
    def index():
        lines = index_path.read_text().splitlines()
        return jsonify(lines)

    return app


# for uWSGI
app = create_app()
