self: pkgs:

{
  test_1 = pkgs.nixosTest {
    name = "test1";
    nodes.machine = {
      services.httpd = {
        enable = true;
        adminAddr = "webmaster@example.org";
        extraModules = ["proxy_uwsgi"];
        virtualHosts."app".locations."/".proxyPass = "unix:/run/uwsgi/uwsgi.sock|uwsgi://%{HTTP_HOST}/";
      };
      services.uwsgi = {
        enable = true;
        group = "wwwrun";
        plugins = [ "python3" ];
        instance.type = "normal";
        instance.pythonPackages = _: [ self.defaultPackage.${pkgs.system}];
        instance.module = "ca_generator:app";
        instance.socket = "/run/uwsgi/uwsgi.sock";
        instance.chmod-socket="660";
        instance.logto="/var/log/uwsgi/uwsgi.log";
      };

      systemd.tmpfiles.rules = [
        "d /var/log/uwsgi 700 uwsgi"
        "d /var/lib/ca_generator_app 755 uwsgi"
      ];

      environment.systemPackages = with pkgs; [ openssl_3 jq ];

      networking.firewall.allowedTCPPorts = [ 80 443 ];

    };
    testScript = ''
      machine.wait_for_open_port(80)

      # get a certificate
      machine.succeed("""
      curl -f http://127.0.0.1/personal_certificate/create --json '{"name": "Jane Doe", "email": "jane@example.org"}' > response
      """)
      # parse it
      machine.succeed("""
      jq -r < response .certificate > cert.pem
      jq -r < response .private_key > key.pem
      """)
      # revoke it
      try:
       machine.succeed("""
      jq -n --rawfile cert cert.pem '{"certificate": $cert}' | curl --json @- http://127.0.0.1/personal_certificate/revoke 
      """)
      finally:
       print(machine.succeed("""
       cat /var/log/uwsgi/uwsgi.log
       """))
    '';
  };
}
