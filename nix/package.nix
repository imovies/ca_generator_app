{python3Packages}:

python3Packages.buildPythonPackage {
  pname = "ca_generator";
  version = "0.1";

  src = builtins.path {
    path = ../.;
    name = "source";
  };

  propagatedBuildInputs = [ python3Packages.flask ];

  format = "pyproject";
  nativeBuildInputs = [ python3Packages.setuptools ];
}
